from django.contrib import admin

# Register your models here.
from sponsor.models import Sponsor, Donation, Reminder, VerificationToken

admin.site.register(Sponsor)
admin.site.register(Donation)
admin.site.register(Reminder)
admin.site.register(VerificationToken)
