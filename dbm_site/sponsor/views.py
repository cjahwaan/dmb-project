from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views import View
from sponsor.models import Sponsor,Donation,VerificationToken
from student.models import Student
from django.contrib.auth.mixins import LoginRequiredMixin as LR
from django.utils import timezone
from django.shortcuts import render
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.conf import settings
# Create your views here.

def index(request,id,token,donation_id):
    #check if token exists
    if VerificationToken.objects.filter(token=token).exists() is False:
        return JsonResponse({'success':False,'Error':'Invalid Token'})
    token_value = VerificationToken.objects.get(token=token)
    #check if token is valid
    if token_value.expiration_date < timezone.now():
        return JsonResponse({'success':False,'Error':'Invalid Token'})
    if Sponsor.objects.filter(id=id).exists() is False:
        return JsonResponse({'success':False,'Error':'Sponsor doesnt exists'})

    if token_value.sponsor != Sponsor.objects.get(id=id):
        return JsonResponse({'success':False,'Error':'Invalid Token'})

    if Donation.objects.filter(id=donation_id).exists() is False:
        return JsonResponse({'success':False,'Error':'Invalid donation'})
    return render(request,'sponsor/html/index.html')

class ReportError(View):
    def post(self,request,token,id,donation_id):
        # check if sponsor exists and is confirmed
        if Sponsor.objects.filter(id=id,confirm=True).exists() is False:
            return JsonResponse({'success':False,'Error':'Sponsor not confirmed'})
        #check if token exists
        if VerificationToken.objects.filter(token=token).exists() is False:
            return JsonResponse({'success':False,'Error':'Token doesnt exists'})
        token_value = VerificationToken.objects.get(token=token)
        #check if token is

        if token_value.expiration_date < timezone.now():
            return JsonResponse({'success':False,'Error':'Invalid Token'})

        if token_value.sponsor != Sponsor.objects.get(id=id):
            return JsonResponse({'success':False,'Error':'Invalid Token'})

        if Donation.objects.filter(id=donation_id).exists() is False:
            return JsonResponse({'success':False,'Error':'Invalid donation'})

        sponsor = Sponsor.objects.get(id=id)
        donation = Donation.objects.get(id=donation_id)
        error_report = request.POST['text']
        student_name = "{} {}".format(donation.student.user.first_name,donation.student.user.last_name)
        amount = donation.amount
        student_id = donation.student.sid

        admins = User.objects.filter(groups__name="Administrator")
        for a in admins:
            subject = 'Error Report'
            message = """Good Afternoon,\n\n
            My name is {} {}, I am a sponsor that wishes to report that
            an error exists with my donation. The error in question is {}.
            For the donation to student {} with id {} of amount {}.\n
            Could you please fix the error so I may proceed with the donation.\n
            \n\nSincerely,\n\n{} {}""".format(sponsor.f_name,sponsor.l_name,
            error_report,student_name,student_id,amount,sponsor.f_name,
            sponsor.l_name)
            to = a.email
            send_mail(
            subject,
            message,
            settings.DEFAULT_EMAIL_FROM,
            [to],
            fail_silently=False
            )

        return JsonResponse({'success':True})

class UpdateSponsor(View):

    # GET = confirms sponsor email
    # takes parameter sponsor_id and token_id
    # returns True if success
    def get(self, request,token,id,donation_id):
        #check if token exists
        if VerificationToken.objects.filter(token=token).exists() is False:
            return JsonResponse({'success':False,'Error':'Invalid Token'})
        token_value = VerificationToken.objects.get(token=token)
        #check if token is valid
        if token_value.expiration_date < timezone.now():
            return JsonResponse({'success':False,'Error':'Invalid Token'})
        if Sponsor.objects.filter(id=id).exists() is False:
            return JsonResponse({'success':False,'Error':'Sponsor doesnt exists'})
        if token_value.sponsor != Sponsor.objects.get(id=id):
            return JsonResponse({'success':False,'Error':'Invalid Token'})
        if Donation.objects.filter(id=donation_id).exists() is False:
            return JsonResponse({'success':False,'Error':'Invalid donation'})
        #update Sponsor
        Sponsor.objects.filter(id=id).update(confirm=True)
        return JsonResponse({"success":True})

class DonationsOp(View):
    # POST = create donation
    # takes parameter donation_id and amount
    # returns True if successful
    def post(self,request,token,id,donation_id):
        # check if sponsor exists and is confirmed
        if Sponsor.objects.filter(id=id,confirm=True).exists() is False:
            return JsonResponse({'success':False,'Error':'Sponsor not confirmed'})
        #check if token exists
        if VerificationToken.objects.filter(token=token).exists() is False:
            return JsonResponse({'success':False,'Error':'Token doesnt exists'})
        token_value = VerificationToken.objects.get(token=token)
        #check if token is

        if token_value.expiration_date < timezone.now():
            return JsonResponse({'success':False,'Error':'Invalid Token'})

        if token_value.sponsor != Sponsor.objects.get(id=id):
            return JsonResponse({'success':False,'Error':'Invalid Token'})

        if Donation.objects.filter(id=donation_id).exists() is False:
            return JsonResponse({'success':False,'Error':'Donation doesnt exist'})
        # get donation
        Donation.objects.filter(id=donation_id).update(completed=True)
        #send to paypal to pay
        return JsonResponse({'success':True})

    # GET = get donation information for student
    # takes parameter donation_id
    # returns donation_amount, student_first_name,student_last_name
    def get(self,request,token,id,donation_id):
        #check if token exists
        if VerificationToken.objects.filter(token=token).exists() is False:
            return JsonResponse({'success':False,'Error':'Invalid Token'})
        token_value = VerificationToken.objects.get(token=token)
        #check if token is valid
        if token_value.expiration_date < timezone.now():
            return JsonResponse({'success':False,'Error':'Invalid Token'})

        if token_value.sponsor != Sponsor.objects.get(id=id):
            return JsonResponse({'success':False,'Error':'Invalid Token'})

        if Donation.objects.filter(id=donation_id).exists() is False:
            return JsonResponse({'success':False,'Error':'donation doesnt exists'})
        donation = Donation.objects.get(id=donation_id)
        student = Student.objects.get(id=donation.student_id)
        return JsonResponse({'success':True,'amount':donation.amount,
        'student_first_name':student.user.first_name,
        'student_last_name':student.user.last_name})
