from django.db import models
from student.models import Student

# Create your models here.
class Sponsor(models.Model):
    f_name = models.CharField(max_length=20)
    l_name = models.CharField(max_length=20)
    email = models.TextField()
    phone = models.CharField(max_length=20)
    confirm = models.BooleanField(default=False)
    thanked = models.BooleanField(default=False)

    def __str__(self):
        return self.f_name + ' ' + self.l_name + ' ' + str(self.confirm)

class Donation(models.Model):
    sponsor = models.ForeignKey(Sponsor, on_delete=models.CASCADE)
    student = models.ForeignKey(Student,on_delete=models.CASCADE)
    amount = models.FloatField()
    date = models.DateField(auto_now_add=True)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return "student_email="+self.student.user.email+"\n"+"sponsor_email="+self.sponsor.email+"\n"+"completed="+str(self.completed)+"\n"+"amount="+str(self.amount)

class Reminder(models.Model):
    sponsor = models.ForeignKey(Sponsor, on_delete=models.CASCADE)
    lastIssue = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sponsor.email+" "+str(self.lastIssue)

class VerificationToken(models.Model):
    issue_date = models.DateTimeField(auto_now_add=True)
    expiration_date = models.DateTimeField()
    sponsor = models.ForeignKey(Sponsor, on_delete=models.CASCADE)
    token = models.TextField()

    def __str__(self):
        return ""+str(self.issue_date)+" "+str(self.expiration_date)
