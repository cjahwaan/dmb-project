# Generated by Django 2.1.1 on 2018-10-07 09:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sponsor', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='verificationtoken',
            old_name='expirationDate',
            new_name='expiration_date',
        ),
        migrations.RenameField(
            model_name='verificationtoken',
            old_name='issueDate',
            new_name='issue_date',
        ),
    ]
