from django.urls import path
from sponsor.views import *

urlpatterns = [
    path('<int:id>/<str:token>/<int:donation_id>',index),
    path('<int:id>/<str:token>/<int:donation_id>/confirm',UpdateSponsor.as_view()),
    path('<int:id>/<str:token>/<int:donation_id>/donation',DonationsOp.as_view()),
    path('<int:id>/<str:token>/<int:donation_id>/error',ReportError.as_view()),
]
