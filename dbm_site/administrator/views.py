from django.http import JsonResponse
import json
from django.views import View as V
from student.models import Student
from administrator.models import Notification
from sponsor.models import Sponsor, Reminder, Donation
from django.contrib.auth.mixins import LoginRequiredMixin as LR
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from datetime import datetime
from django.db.models import Sum
from sponsor.models import VerificationToken
from django.utils.crypto import get_random_string
from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.conf import settings
import csv
import os
# gets all students, and creates a student
class StudentOp(LR,V):
    login_url='/auth'

    # GET = get all students
    # takes no params
    # returns list of dictionary of students
    def get(self, request,f,t):
        f = int(f)
        t = int(t)
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        if Student.objects.all().exists() is False:
            return JsonResponse({'success':False,'error':'There are no students'})

        # get reference to students
        student = Student.objects.all().values('user__first_name', 'user__last_name', 'user__email', 'phone','sid','id')[f:t]

        if len(student)==0:
            return JsonResponse({'success':False,'Error':'went over min or max'})

        return JsonResponse(
            {
                'success':True,
                'students':json.dumps(list(student))
            }
        )

    # POST = add student record
    # takes parameter f_name,l_name,email,phone
    # returns True if it worked
    def post(self, request):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        #student = Student.objects.get().values('f_name', 'l_name', 'email', 'phone')
        #f_name, l_name, email, phone
        f_name = request.POST['first_name']
        l_name = request.POST['last_name']
        email = request.POST['email']
        phone = request.POST['phone']
        sid = request.POST['sid']

        #check if user exists
        if User.objects.filter(email=email).exists() is True:
            return JsonResponse({'success':False,'Error':'User already exists'})

        # create user
        user = User(
        username=email,
        email=email,
        first_name=f_name,
        last_name=l_name
        )
        #set default password
        user.set_password('password')
        user.save()

        # get user reference
        user = User.objects.get(email=email)

        group = Group.objects.get(name="Student")
        group.user_set.add(user)

        #check if student already exists
        if Student.objects.filter(user=user).exists() is True:
            return JsonResponse({'success':False,'Error':'Student already exists in file'})
        if Student.objects.filter(sid=sid).exists() is True:
            return JsonResponse({'success':False,'Error':'sid already exists'})
        # create student object
        student = Student(
            phone = phone,
            user = user,
            sid=sid
        )
        # save student to database
        student.save()
        return JsonResponse(
            {
                'success':True
            }
        )

class GetStudentByStudentID(LR,V):
    login_url='/auth'

    # GET = returns list of dictionary of students
    # takes params student_id
    def get(self,request,sid):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        # check if there are students
        sid = int(sid)
        if Student.objects.filter(sid=sid).exists() is False:
            return JsonResponse({'success':False,'Error':'Search returned no results'})
        student = Student.objects.filter(sid=sid).values('user__email','user__first_name',
        'user__last_name','phone','id','sid')
        return JsonResponse({
        'success':True,
        'student':json.dumps(list(student))
        })

class GetAllSponsors(LR,V):
    login_url='/auth'

    # GET = get all sponsors
    # takes no parms
    # returns list of dictionaries of sponsors
    def get(self, request,f,t):
        f = int(f)
        t = int(t)
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        if Sponsor.objects.all().count() ==0:
            return JsonResponse({'success':False,'Error':'There are no sponsors'})

        # gets sponsor objects
        sponsor = Sponsor.objects.all().values('f_name', 'l_name', 'email', 'confirm','id','thanked')[f:t]

        # check if there are sponsors
        if len(sponsor)==0:
            return JsonResponse({'success':False,'error':'went over max or min'})

        #if there are return list of dictionary
        return JsonResponse(
            {
                'success':True,
                'sponsor':json.dumps(list(sponsor),default=str)
            }
        )

# get = get unconfirmed sponsors
# post = send email to uncofirmed sponsors
class UnconfirmedSponsors(LR,V):
    login_url='/auth'

    # POST = send email to unconfirmed sponsors
    # takes params template selection
    # returns True if successful
    def get(self,request):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        # get sponsors
        sponsor = Sponsor.objects.filter(confirm=False)
        # check if there are any
        if len(sponsor)==0:
            return JsonResponse({'success':False,'Error':'There are no uncofirmed sponsors'})
        for s in sponsor:
            # check if reminder already sent
            if Reminder.objects.filter(sponsor=s).exists() is True:
                reminder = Reminder.objects.get(sponsor=s)
                reminder.lastIssue=datetime.now()
                reminder.save()
            else:
                # create reminder
                reminder = Reminder(sponsor=s)
                reminder.save()

            #create new verification token
            unique_id = get_random_string(length=32)
            now = datetime.now()
            offset = now+timedelta(days=2)

            token = VerificationToken(
            expiration_date= offset,
            sponsor=s,
            token=unique_id
            )
            token.save()
            donation = Donation.objects.filter(sponsor=s,completed=False)
            # send email
            for d in donation:
                link = 'localhost:8000/sponsor/{}/{}/{}'.format(s.id,unique_id,d.id)
                message = """Dear {} {},\n\nI am a member of Keller High School Marching band. We were invited to march in the Macy's Day
                Thanksgiving Paradate - a very prestigious honor. I have been asked to contact my supporters to help me get to New York City.
                You may send a check to KHS Band, 601 S. Pate Orr Road, Keller, TX 76248.\n\n
                Thank you for supporting the Keller High School Marching Band.\n
                Please use the following link to confirm your donation {}.\n
                Sincerely,\n
                Keller High School Marching Band""".format(s.f_name,s.l_name,link)
                subject = 'Reminder: Confirmation Email'
                to = s.email
                send_mail(
                subject,
                message,
                settings.DEFAULT_EMAIL_FROM,
                [to],
                fail_silently=False
                )

        return JsonResponse({'success':True})

class UnconfirmedSponsor(LR,V):
    login_url='/auth'
    def get(self,request,id):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        # check if there are any
        if Sponsor.objects.filter(id=id).exists() is False:
            return JsonResponse({'success':False,'Error':'This sponsor doesn\'t exists'})
        # get sponsors
        sponsor = Sponsor.objects.get(id=id)


        if Reminder.objects.filter(sponsor=sponsor).exists() is True:
            reminder = Reminder.objects.get(sponsor=sponsor)
            reminder.lastIssue=datetime.now()
            reminder.save()
        else:
            # create reminder
            reminder = Reminder(sponsor=sponsor)
            reminder.save()

        #create new verification token
        unique_id = get_random_string(length=32)
        now = datetime.now()
        offset = now+timedelta(days=2)

        token = VerificationToken(
        expiration_date= offset,
        sponsor=sponsor,
        token=unique_id
        )
        token.save()
        # send email

        donations = Donation.objects.filter(sponsor=sponsor,completed=False)

        for d in donations:
            link = 'localhost:8000/sponsor/{}/{}/{}'.format(sponsor.id,unique_id,d.id)
            message = """Dear {} {},\n\nI am a member of Keller High School Marching band. We were invited to march in the Macy's Day
            Thanksgiving Paradate - a very prestigious honor. I have been asked to contact my supporters to help me get to New York City.
            You may send a check to KHS Band, 601 S. Pate Orr Road, Keller, TX 76248.\n\n
            Thank you for supporting the Keller High School Marching Band.\n
            Please use the following link to confirm your donation {}.\n
            Sincerely,\n
            Keller High School Marching Band""".format(sponsor.f_name,sponsor.l_name,link)
            subject = 'Reminder: Confirmation Email'
            to = sponsor.email
            send_mail(
            subject,
            message,
            settings.DEFAULT_EMAIL_FROM,
            [to],
            fail_silently=False
            )

        return JsonResponse({'success':True})

class ConfirmedSponsors(LR,V):
    login_url='/auth'

    def get(self,request):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        # get sponsors
        sponsor = Sponsor.objects.filter(confirm=True,thanked=False)
        # check if there are any
        if len(sponsor)==0:
            return JsonResponse({'success':False,'Error':'There are no uncofirmed sponsors'})
        for s in sponsor:
            # send email
            message = """Dear {} {},\n\n Thank you for taking my call and helping me get to New York City.
            We're marching in the Macy's Day Thanksgiving Parade -- a very prestigious honor. Your pledge will
            help us greatly.\n\nThank you for supporting the Keller High School Marching Band.\n
            Sincerely,\nKeller High School Marching Band""".format(s.f_name,s.l_name)
            subject = 'Thank You Email'
            to = s.email
            send_mail(
            subject,
            message,
            settings.DEFAULT_EMAIL_FROM,
            [to],
            fail_silently=False
            )
            s.thanked = True
            s.save()

        return JsonResponse({'success':True})

class ConfirmedSponsor(LR,V):
    login_url='/auth'

    def get(self,request,id):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        # check if there are any
        if Sponsor.objects.filter(id=id).exists() is False:
            return JsonResponse({'success':False,'Error':'This sponsor doesnt exists'})

        # get sponsors
        sponsor = Sponsor.objects.get(id=id)


        # send email
        message = """Dear {} {},\n\n Thank you for taking my call and helping me get to New York City.
        We're marching in the Macy's Day Thanksgiving Parade -- a very prestigious honor. Your pledge will
        help us greatly.\n\nThank you for supporting the Keller High School Marching Band.\n
        Sincerely,\nKeller High School Marching Band""".format(sponsor.f_name,sponsor.l_name)
        subject = 'Thank You Email'
        to = sponsor.email
        send_mail(
        subject,
        message,
        settings.DEFAULT_EMAIL_FROM,
        [to],
        fail_silently=False
        )

        return JsonResponse({'success':True})

class GetSponsorByName(LR,V):
    login_url='/auth'

    def get(self,request,l_name):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        # get sponsors
        sponsors = Sponsor.objects.filter(l_name__startswith=l_name).values(
        'f_name','l_name','email','phone','confirm','thanked','id'
        )
        #check if there are any
        if len(sponsors)==0:
            return JsonResponse({'success':False,'Error':'There are no matches for search'})

        return JsonResponse({'success':True,'sponsors':json.dumps(list(sponsors),default=str)})

class UpdateSponsor(LR,V):
    login_url='/auth'

    def get(self,request,id):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        if Sponsor.objects.filter(id=id).exists() is False:
            return JsonResponse({'success':False,'Error':'Sponsor doesnt exists'})

        sponsor = Sponsor.objects.get(id=id)
        return JsonResponse({
        'success':True,
        'f_name':sponsor.f_name,
        'l_name':sponsor.l_name,
        'email':sponsor.email
        })

    # POST = update sponsor
    # takes params sponsor_id,f_name, l_name, email, phone
    # returns True if successful
    def post(self,request,id):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        f_name = request.POST['f_name']
        l_name = request.POST['l_name']
        email = request.POST['email']
        #check if sponsor exists
        if Sponsor.objects.filter(id=id).exists() is False:
            return JsonResponse({'success':False,'Error':'Sponsor doesnt exists'})
        #update sponsor
        Sponsor.objects.filter(id=id).update(
        f_name=f_name,l_name=l_name,email=email
        )
        return JsonResponse({'success':True})

class DownloadSponsorCSV(LR,V):
    login_url='/auth'
    def get(self,request):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        if Sponsor.objects.all().count() ==0:
            return JsonResponse({'success':False,'Error':'There are no sponsors'})

        # gets sponsor objects
        sponsor = Sponsor.objects.all().values('f_name', 'l_name', 'email', 'confirm','id','thanked')

        try:
            os.remove('media/sponsor.csv')
        except OSError:
            pass

        with open('media/sponsor.csv','w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['first_name','last_name','email','confirm','thanked'])
            for s in sponsor:
                if s['confirm'] is True:
                    c = "True"
                elif s['confirm'] is False:
                    c = "False"
                if s['thanked'] is True:
                    d = "True"
                elif s['thanked'] is False:
                    d = "False"

                l = [s['f_name'],s['l_name'],s['email'],c,d]
                writer.writerow(l)

        return JsonResponse({'success':True,'path':'sponsor.csv'})

class DownloadSponsorDonationCSV(LR,V):
    login_url='/auth'
    def get(self,request,id):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        if Sponsor.objects.all().count() ==0:
            return JsonResponse({'success':False,'Error':'There are no sponsors'})

        sponsor = Sponsor.objects.get(id=id)

        donation = Donation.objects.filter(sponsor=sponsor).values('amount',
        'date','completed',
        'student__user__first_name','student__user__last_name')

        try:
            os.remove('media/sponsordonation.csv')
        except OSError:
            pass

        with open('media/sponsordonation.csv','w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['date','amount','student first name','student last name','completed'])
            for d in donation:
                if d['completed'] is True:
                    c = "True"
                elif d['completed'] is False:
                    c = "False"

                l = [d['date'],d['amount'],d['student__user__first_name'],d['student__user__last_name'],c]
                writer.writerow(l)

        return JsonResponse({'success':True,'path':'sponsordonation.csv'})

class DownloadSponsorStudentCSV(LR,V):
    login_url='/auth'
    def get(self,request,id):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'error':'Unauthorized User'})

        if Sponsor.objects.filter(id=id).exists() is False:
            return JsonResponse({'success':False,'error':'sponsor doesnt exists'})

        sponsor = Sponsor.objects.get(id=id)
        student = Donation.objects.filter(sponsor=sponsor).values('amount',
        'date','student__user__first_name','student__user__last_name')
        if len(student)==0:
            return JsonResponse({'success':False,'error':'this sponsor has no students'})

        try:
            os.remove('media/sponsor_student.csv')
        except OSError:
            pass

        with open('media/sponsor_student.csv','w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['date','student first name','student last name'])
            for s in student:

                l = [s['date'],s['student__user__first_name'],s['student__user__last_name']]
                writer.writerow(l)

        return JsonResponse({'success':True,'path':'sponsor_student.csv'})

class DownloadStudentCSV(LR,V):
    login_url='/auth'
    def get(self,request):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        if Student.objects.all().exists() is False:
            return JsonResponse({'success':False,'error':'There are no students'})

        # get reference to students
        student = Student.objects.all().values('user__first_name', 'user__last_name',
        'user__email', 'phone','sid','id')

        try:
            os.remove('media/student.csv')
        except OSError:
            pass

        with open('media/student.csv','w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['first name','last name','email','phone','sid'])
            for s in student:

                l = [s['user__first_name'],s['user__last_name'],
                s['user__email'],s['phone'],s['sid']]
                writer.writerow(l)

        return JsonResponse({'success':True,'path':'student.csv'})

class DownloadStudentSponsor(LR,V):
    login_url='/auth'
    def get(self,request,id):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        student_id = id

        # check if student exists
        if Student.objects.filter(id=student_id).exists() is False:
            return JsonResponse({'success':False,'Error':'Student does not exists'})

        #get student object
        student = Student.objects.get(id=student_id)

        if Donation.objects.filter(student=student).exists() is False:
            return JsonResponse({'success':False,'Error':'This student has no donation'})

        sponsors = Donation.objects.filter(student=student).distinct().values(
        'sponsor__f_name',
        'sponsor__l_name',
        'sponsor__email')

        try:
            os.remove('media/studentsponsor.csv')
        except OSError:
            pass

        with open('media/studentsponsor.csv','w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['sponsor first name','sponsor last name','sponsor email'])
            for s in sponsors:
                l = [s['sponsor__f_name'],s['sponsor__l_name'],s['sponsor__email']]
                writer.writerow(l)

        return JsonResponse({'success':True,'path':'studentsponsor.csv'})

class DownloadStudentDonations(LR,V):
    login_url='/auth'
    def get(self,request,sid):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        student_id = sid

        # check if student exists
        if Student.objects.filter(id=student_id).exists() is False:
            return JsonResponse({'success':False,'Error':'Student does not exists'})

        #get student object
        student = Student.objects.get(id=student_id)

        if Donation.objects.filter(student=student).exists() is False:
            return JsonResponse({'success':False,'Error':'This student has no donation'})

        # get donations for student
        donation = Donation.objects.filter(student=student).values('amount', 'date',
         'completed',
        'sponsor__f_name','sponsor__l_name','sponsor__email')

        try:
            os.remove('media/student_donation.csv')
        except OSError:
            pass

        with open('media/student_donation.csv','w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['date','sponsor first name','sponsor last name',
            'sponsor email','amount','completed'])
            for d in donation:
                if d['completed'] is True:
                    c = "True"
                elif d['completed'] is False:
                    c = "False"
                l = [d['date'],d['sponsor__f_name'],d['sponsor__l_name'],
                d['sponsor__email'],d['amount'],c]
                writer.writerow(l)

        return JsonResponse({'success':True,'path':'student_donation.csv'})

# GET = get all Notifications
# POST = create notifications
class NotificationsOp(LR,V):
    login_url='/auth'

    # takes no params
    # returns list of ditionaries of Notifications
    def get(self,request):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        # get notifications
        notifications = Notification.objects.all().values('sender__first_name','sender__last_name',
        'message','date')

        #check if there are any
        if len(notifications)==0:
            return JsonResponse({'success':False,'Error':'There are no notifications'})

        return JsonResponse({'success':True,'notifications':json.dumps(list(notifications),default=str)})

    # create notification
    # takes params message
    # returns True if success
    def post(self,request):
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        message = request.POST['message']
        user = request.user

        #saves notification
        notif = Notification(
        sender=user,
        message=message
        )
        notif.save()
        return JsonResponse({'success':True})

class GetSponsorForStudent(LR,V):
    login_url='/auth'
    def get(self,request,id,f,t):
        f = int(f)
        t = int(t)
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        student_id = id

        # check if student exists
        if Student.objects.filter(id=student_id).exists() is False:
            return JsonResponse({'success':False,'Error':'Student does not exists'})

        #get student object
        student = Student.objects.get(id=student_id)

        if Donation.objects.filter(student=student).exists() is False:
            return JsonResponse({'success':False,'Error':'This student has no donation'})

        sponsors = Donation.objects.filter(student=student).distinct().values('sponsor__f_name','sponsor__l_name',
        'sponsor__email')[f:t]
        if len(sponsors)==0:
            return JsonResponse({'success':False,'error':'went over min or max'})
        return JsonResponse({'success':True,'sponsors':json.dumps(list(sponsors))})

class GetDonationsForStudent(LR,V):
    login_url='/auth'

    # GET = get donations for student
    # takes params student_id
    # returns list of dictionary of donations
    def get(self, request,sid,f,t):
        f = int(f)
        t = int(t)
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        student_id = sid

        # check if student exists
        if Student.objects.filter(id=student_id).exists() is False:
            return JsonResponse({'success':False,'Error':'Student does not exists'})

        #get student object
        student = Student.objects.get(id=student_id)

        if Donation.objects.filter(student=student).exists() is False:
            return JsonResponse({'success':False,'Error':'This student has no donation'})

        # get donations for student
        donation = Donation.objects.filter(student=student).values('amount', 'date', 'completed',
        'sponsor__f_name','sponsor__l_name','sponsor__email')[f:t]
        if len(donation)==0:
            return JsonResponse({'success':False,'error':'Went over min or max'})
        return JsonResponse(
            {
                'success':True,
                'donation':json.dumps(list(donation),default=str)
            }
        )

class GetDonationsForSponsorStudent(LR,V):
    login_url='/auth'
    def get(self,request,sid,f,t):
        f=int(f)
        t= int(t)
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'error':'Unauthorized User'})

        if Sponsor.objects.filter(id=sid).exists() is False:
            return JsonResponse({'success':False,'error':'sponsor doesnt exists'})

        sponsor = Sponsor.objects.get(id=sid)
        if Donation.objects.filter(sponsor=sponsor).exists() is False:
            return JsonResponse({'success':False,'error':'This sponsor has no students'})
        student = Donation.objects.filter(sponsor=sponsor).values('amount','date','student__user__first_name','student__user__last_name')[f:t]
        if len(student)==0:
            return JsonResponse({'success':False,'error':'went over min or max'})

        return JsonResponse({'success':True,'student':json.dumps(list(student),default=str)})

class GetDonationsForSponsor(LR,V):
    login_url='/auth'
    def get(self,request,sid,f,t):
        f = int(f)
        t = int(t)
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'error':'Unauthorized User'})

        if Sponsor.objects.filter(id=sid).exists() is False:
            return JsonResponse({'success':False,'error':'sponsor doesnt exists'})

        sponsor = Sponsor.objects.get(id=sid)

        if Donation.objects.filter(sponsor=sponsor).exists() is False:
            return JsonResponse({'success':False,'error':'This sponsor has no donations'})

        donation = Donation.objects.filter(sponsor=sponsor).values('amount','date','completed','student__user__first_name','student__user__last_name')[f:t]
        if len(donation)==0:
            return JsonResponse({'success':False,'error':'went over max or min'})
        return JsonResponse({'success':True,'donation':json.dumps(list(donation),default=str)})

class GetDonationRatio(LR,V):
    login_url='/auth'

    # returns unconfirmed amount, total amount
    # takes no params
    def get(self,request):
        goal = 10000000
        if request.user.groups.filter(name='Administrator').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        if Donation.objects.all().exists() is False:
            return JsonResponse({'success':False,'Error':'There are no donations'})

        unconfirmed = Donation.objects.filter(completed=False).aggregate(Sum('amount'))
        if unconfirmed['amount__sum'] is None:
            unconfirmed = 0
        else:
            unconfirmed = unconfirmed['amount__sum']
        total = Donation.objects.all().aggregate(Sum('amount'))
        total = total['amount__sum']
        tot_sponsor = Sponsor.objects.count()
        tot_unconfirmed_sponsor = Sponsor.objects.filter(confirm=False).count()
        return JsonResponse({
        'success':True,
        'unconfirmed':unconfirmed,
        'total':total,
        'goal':goal,
        'tot_sponsor':tot_sponsor,
        'tot_unconfirmed_sponsor':tot_unconfirmed_sponsor
        })
