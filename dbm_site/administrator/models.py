from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Notification(models.Model):
    sender = models.ForeignKey(User,on_delete=models.CASCADE)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sender.first_name+" "+self.sender.last_name+" "+self.message
