# Generated by Django 2.1.1 on 2018-11-14 19:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administrator', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='passwordchangetoken',
            name='user',
        ),
        migrations.DeleteModel(
            name='PasswordChangeToken',
        ),
    ]
