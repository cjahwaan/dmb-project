from django.urls import path
from administrator.views import *

#administrator/
urlpatterns = [
    path('student/<int:f>/<int:t>',StudentOp.as_view()),
    path('student/find/<str:sid>',GetStudentByStudentID.as_view()),
    path('student/donations/<int:sid>/<int:f>/<int:t>',GetDonationsForStudent.as_view()),
    path('student/donations/<int:sid>/csv',DownloadStudentDonations.as_view()),
    path('student/sponsor/<int:id>/<int:f>/<int:t>',GetSponsorForStudent.as_view()),
    path('student/sponsor/<int:id>/csv',DownloadStudentSponsor.as_view()),
    path('student/csv',DownloadStudentCSV.as_view()),
    path('sponsor/csv',DownloadSponsorCSV.as_view()),
    path('sponsor/<int:f>/<int:t>',GetAllSponsors.as_view()),
    path('sponsor/unconfirmed',UnconfirmedSponsors.as_view()),
    path('sponsor/unconfirmed/<int:id>',UnconfirmedSponsor.as_view()),
    path('sponsor/confirmed',ConfirmedSponsors.as_view()),
    path('sponsor/confirmed/<int:id>',ConfirmedSponsor.as_view()),
    path('sponsor/find/<str:l_name>',GetSponsorByName.as_view()),
    path('sponsor/update/<str:id>',UpdateSponsor.as_view()),
    path('sponsor/donation/<int:sid>/<int:f>/<int:t>',GetDonationsForSponsor.as_view()),
    path('sponsor/student/<int:sid>/<int:f>/<int:t>',GetDonationsForSponsorStudent.as_view()),
    path('sponsor/student/<int:id>/csv',DownloadSponsorStudentCSV.as_view()),
    path('sponsor/donation/<int:id>/csv',DownloadSponsorDonationCSV.as_view()),
    path('notifications',NotificationsOp.as_view()),
    path('donations',GetDonationRatio.as_view()),
]
