from django.urls import path
from student.views import *

urlpatterns = [
    path('notif',GetNofitication.as_view()),
    path('sponsor/<int:f>/<int:t>',SponsorOp.as_view()),
    path('donations',GetDonationRatio.as_view()),
    path('download',DownloadSponsorStudentCSV.as_view()),
    path('notifications',NotificationsOp.as_view())
]
