from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Student(models.Model):
    phone = models.CharField(max_length=40)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    sid = models.IntegerField()

    def __str__(self):
        return self.user.first_name+" "+self.user.last_name
