from django.http import JsonResponse
import json
from django.views import View
from sponsor.models import Sponsor,Donation
from administrator.models import Notification
from django.db.models import Sum
from django.contrib.auth.mixins import LoginRequiredMixin as LR
from student.models import Student
from sponsor.models import VerificationToken
from django.utils.crypto import get_random_string
from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.conf import settings
import csv
import os
# Create your views here.


class GetDonationRatio(LR,View):
    login_url='/auth'

    # returns unconfirmed amount, total amount
    # takes no params
    def get(self,request):
        goal = 10000000
        if request.user.groups.filter(name='Student').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})

        if Donation.objects.all().exists() is False:
            return JsonResponse({'success':False,'Error':'There are no donations'})

        unconfirmed = Donation.objects.filter(completed=False).aggregate(Sum('amount'))
        if unconfirmed['amount__sum'] is None:
            unconfirmed = 0
        else:
            unconfirmed = unconfirmed['amount__sum']
        total = Donation.objects.all().aggregate(Sum('amount'))
        total = total['amount__sum']
        tot_sponsor = Sponsor.objects.count()
        tot_unconfirmed_sponsor = Sponsor.objects.filter(confirm=False).count()
        return JsonResponse({
        'success':True,
        'unconfirmed':unconfirmed,
        'total':total,
        'goal':goal,
        'tot_sponsor':tot_sponsor,
        'tot_unconfirmed_sponsor':tot_unconfirmed_sponsor
        })

class GetNofitication(LR,View):
    login_url = '/auth'

    # GET = get nofications
    # takes no params
    # returns list of dictionaries of Notification
    def get(self,request):
        if request.user.groups.filter(name='Student').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        # get nofitications
        notifications = Notification.objects.all().values('sender','message','date')
        #check if there are notifications
        if len(notifications)==0:
            return JsonResponse({'success':False,'Error':'There are no notifications'})

        return JsonResponse({'success':True,'notifications':json.dumps(list(notifications),default=str)})

class DownloadSponsorStudentCSV(LR,View):
    login_url='/auth'

    def get(self,request):
        if request.user.groups.filter(name='Student').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        if Student.objects.filter(user=request.user).exists() is False:
            return JsonResponse({'success':False,'Error':'You are not a student'})

        student = Student.objects.get(user=request.user)

        if Donation.objects.filter(student=student).all().exists() is False:
            return JsonResponse({'success':False,'error':'There are no donations for this student'})

        #get sponsors
        sponsors = Donation.objects.filter(student=student).values('sponsor__f_name','sponsor__l_name',
        'sponsor__email','sponsor__phone','sponsor__confirm','amount','date')

        #check if there are sponsors
        if len(sponsors)==0:
            return JsonResponse({'success':False,'Error':'You have no sponsors'})

        try:
            os.remove('media/student_sponsor.csv')
        except OSError:
            pass

        with open('media/student_sponsor.csv','w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['first_name','last_name','email','phone','confirm','amount','date'])
            for s in sponsors:
                if s['sponsor__confirm'] is True:
                    c = "True"
                elif s['sponsor__confirm'] is False:
                    c = "False"
                l = [s['sponsor__f_name'],s['sponsor__l_name'],s['sponsor__email'],s['sponsor__phone'],
                c,s['amount'],s['date']]
                writer.writerow(l)

        return JsonResponse({'success':True,'path':'student_sponsor.csv'})

class SponsorOp(LR,View):
    login_url='/auth'

    # GET = get sponsors for student
    # takes no params
    # return list of dictionaries of Sponsors
    def get(self,request,f,t):
        f = int(f)
        t = int(t)
        if request.user.groups.filter(name='Student').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        if Student.objects.filter(user=request.user).exists() is False:
            return JsonResponse({'success':False,'Error':'You are not a student'})
        # get student from currently logged in user
        student = Student.objects.get(user=request.user)

        if Donation.objects.filter(student=student).all().exists() is False:
            return JsonResponse({'success':False,'error':'There are no donations for this student'})

        #get sponsors
        sponsors = Donation.objects.filter(student=student).values('sponsor__f_name','sponsor__l_name',
        'sponsor__email','sponsor__phone','sponsor__confirm','amount','date')[f:t]
        #check if there are sponsors
        if len(sponsors)==0:
            return JsonResponse({'success':False,'Error':'You have no sponsors'})

        return JsonResponse({'success':True,'sponsors':json.dumps(list(sponsors),default=str)})

    # POST = create sponsor
    # takes params f_name, l_name, email, amount
    # returns True if successful
    def post(self,request,f,t):
        if request.user.groups.filter(name='Student').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        fname = request.POST['f_name']
        lname= request.POST['l_name']
        email = request.POST['email']
        phone = request.POST['phone']
        amount = request.POST['amount']

        #check if sponsor exists
        if Sponsor.objects.filter(email=email).exists() is True:
            # get student for currently logged in user
            student = Student.objects.get(user=request.user)

            # get sponsor
            sponsor = Sponsor.objects.get(email=email)
            # create donation
            donation = Donation(
            sponsor = sponsor,
            student= student,
            amount = amount
            )
            #save donation
            donation.save()
            return JsonResponse({'success':True})

        #create sponsor object
        sponsor = Sponsor(
            f_name =fname,
            l_name = lname,
            email = email
        )

        student = Student.objects.get(user=request.user)
        #save object to database
        sponsor.save()
        sponsor = Sponsor.objects.get(email=email)
        #create donation
        donation = Donation(
        sponsor = sponsor,
        student= student,
        amount = amount
        )
        #save donation
        donation.save()
        # create verification token
        unique_id = get_random_string(length=32)
        now = datetime.now()
        offset = now + timedelta(days=2)

        token = VerificationToken(
        expiration_date = offset,
        sponsor=sponsor,
        token = unique_id
        )
        token.save()
        # send email
        link = 'localhost:8000/sponsor/{}/{}/{}'.format(sponsor.id,unique_id,donation.id)
        message = """Dear {} {},\n\nI am a member of Keller High School Marching band. We were invited to march in the Macy's Day
        Thanksgiving Paradate - a very prestigious honor. I have been asked to contact my supporters to help me get to New York City.
        You may send a check to KHS Band, 601 S. Pate Orr Road, Keller, TX 76248.\n\n
        Thank you for supporting the Keller High School Marching Band.\n
        Please use the following link to confirm your donation {}.\n
        Sincerely,\n
        Keller High School Marching Band""".format(sponsor.f_name,sponsor.l_name,link)
        subject = 'Confirmation Email'
        to = sponsor.email
        send_mail(
        subject,
        message,
        settings.DEFAULT_EMAIL_FROM,
        [to],
        fail_silently=False
        )
        return JsonResponse({'success':True})

# GET = get all Notifications
# POST = create notifications
class NotificationsOp(LR,View):
    login_url='/auth'

    # takes no params
    # returns list of ditionaries of Notifications
    def get(self,request):
        if request.user.groups.filter(name='Student').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        # get notifications
        notifications = Notification.objects.all().values('sender__first_name','sender__last_name',
        'message','date')

        #check if there are any
        if len(notifications)==0:
            return JsonResponse({'success':False,'Error':'There are no notifications'})

        return JsonResponse({'success':True,'notifications':json.dumps(list(notifications),default=str)})

    # create notification
    # takes params message
    # returns True if success
    def post(self,request):
        if request.user.groups.filter(name='Student').exists() is False:
            return JsonResponse({'success':False,'Error':'Unauthorized User'})
        message = request.POST['message']
        user = request.user

        #saves notification
        notif = Notification(
        sender=user,
        message=message
        )
        notif.save()
        return JsonResponse({'success':True})
