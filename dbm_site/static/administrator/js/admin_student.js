$(document).ready(()=>{

  $.ajax({
    type:'GET',
    url:'administrator/student/0/2',
    success:(data)=>{
      if(data.success){
        var students = JSON.parse(data.students);
        $('#student-table-rows').empty();
        students.forEach((obj)=>{
          var html = '';
          html+='<tr onclick="generate_modal(\''+obj.id+'\')">';
            html+='<td>'+obj.sid+'</td>';
            html+='<td>'+obj.user__first_name+'</td>';
            html+='<td>'+obj.user__last_name+'</td>';
            html+='<td>'+obj.user__email+'</td>';
            html+='<td>'+obj.phone+'</td>';
          html+='</tr>';
          $('#student-table-rows').append(html);
        });
        $('#student-table-rows').find('tr').click((e)=>{
          var selected = $(e.target).closest('tr').hasClass('student-table-active');
          console.log(selected);
          $('#student-table-rows tr').removeClass('student-table-active');
          if(!selected){
              $(e.target).closest('tr').addClass('student-table-active');
              $('#myModal').modal('toggle');
              $('#myModal').modal('show');
              $('#modal-title').empty();
              $('#modal-title').append('Select an option');
              var html='<div class="text-center">';
                html+='<button type="button" style="font-size:large;" onclick="view_donations_student(\'0\',\'2\',\'1\')" class="btn btn-default">View Donations</button>';
                html+='<button type="button" style="font-size:large;" onclick="view_sponsors(\'0\',\'2\',\'1\')" class="btn btn-default">View Sponsors</button>';
              html+='</div>';
              $('#modal-body').empty();
              $('#modal-body').append(html);
              //$('#myModal').modal('hide');
          }
        });
      }
    },
    error:(e,s)=>{
      console.log(s);
    }
  });

  $('#search-btn').click(function(){
    var text = $('#search-text').val();
    $('#search-text').val("");
    if(text==""){
      alert('search cant be empty');
    }else{
      //get rows from backend
      $.ajax({
        type:'GET',
        url:'administrator/student/find/'+text,
        success:(data)=>{
          if(data.success){
            var d = JSON.parse(data.student);
            $('#student-table-rows').empty();
            d.forEach((obj)=>{
              var html='';
              html+='<tr onclick="generate_modal(\''+obj.id+'\')">';
                html+='<td>'+obj.sid+'</td>';
                html+='<td>'+obj.user__first_name+'</td>';
                html+='<td>'+obj.user__last_name+'</td>';
                html+='<td>'+obj.user__email+'</td>';
                html+='<td>'+obj.phone+'</td>';
              html+='</tr>';
              $('#student-table-rows').append(html);
            });
            $('#student-table-rows').find('tr').click((e)=>{
              var selected = $(e.target).closest('tr').hasClass('student-table-active');
              console.log(selected);
              $('#student-table-rows tr').removeClass('student-table-active');
              if(!selected){
                  $(e.target).closest('tr').addClass('student-table-active');
                  $('#myModal').modal('toggle');
                  $('#myModal').modal('show');
                  $('#modal-title').empty();
                  $('#modal-title').append('Select an option');
                  var html='<div class="text-center">';
                    html+='<button type="button" style="font-size:large;" onclick="view_donations_student(\'0\',\'2\',\'1\')" class="btn btn-default">View Donations</button>';
                    html+='<button type="button" style="font-size:large;" onclick="view_sponsors(\'0\',\'2\',\'1\')" class="btn btn-default">View Sponsors</button>';
                  html+='</div>';
                  $('#modal-body').empty();
                  $('#modal-body').append(html);
                  //$('#myModal').modal('hide');
              }
            });
          }else{
            alert('search returned no results');
          }
        },
        error:(e,s)=>{
          alert('search returned no results');
          console.log(s);
        }
      });
    }
  });

  $('#download-btn').click(function(){
    $.ajax({
      type:'GET',
      url:'administrator/student/csv',
      success:(data)=>{
        if(data.success){
          window.location.href='/media/'+data.path;
        }else{
          alert('error downloading file');
        }
      },
      error:(e,s)=>{
        console.log(s);
        alert('error downloading file');
      }
    });
  });

  $('#student-table-columns th').each(function(col){
    $(this).hover(function(){$(this).addClass('focus');},function(){$(this).removeClass('focus')});
    $(this).click(function(){
      if($(this).is('.asc')){
        $(this).removeClass('asc');
        $(this).addClass('desc selected');
        sortOrder=-1;
      }else{
        $(this).addClass('asc selected');
        $(this).removeClass('desc');
        sortOrder=1;
      }
      $(this).siblings().removeClass('asc selected');
      $(this).siblings().removeClass('desc selected');
      var arrData = $('table').find('tbody >tr:has(td)').get();
      arrData.sort(function(a,b){
        var val1 = $(a).children('td').eq(col).text().toUpperCase();
        var val2 = $(b).children('td').eq(col).text().toUpperCase();
        if($.isNumeric(val1) && $.isNumeric(val2)){
          return sortOrder == 1 ? val1-val2:val2-val1;
        }else{
          return (val1 < val2) ? -sortOrder : (val1>val2)? sortOrder:0;
        }
      });
      $.each(arrData, function(index,row){
        $('tbody').append(row);
      });
    });
  });
});

function get_student(from,to){
  from = parseInt(from);
  to = parseInt(to);
  $.ajax({
    type:'GET',
    url:'administrator/student/'+from+'/'+to,
    success:(data)=>{
      if(data.success){
        const offset = 2;
        var students = JSON.parse(data.students);
        $('#student-table-rows').empty();
        students.forEach((obj)=>{
          var html = '';
          html+='<tr onclick="generate_modal(\''+obj.id+'\')">';
            html+='<td>'+obj.sid+'</td>';
            html+='<td>'+obj.user__first_name+'</td>';
            html+='<td>'+obj.user__last_name+'</td>';
            html+='<td>'+obj.user__email+'</td>';
            html+='<td>'+obj.phone+'</td>';
          html+='</tr>';
          $('#student-table-rows').append(html);
        });
        $('#student-table-rows').find('tr').click((e)=>{
          var selected = $(e.target).closest('tr').hasClass('student-table-active');
          console.log(selected);
          $('#student-table-rows tr').removeClass('student-table-active');
          if(!selected){
              $(e.target).closest('tr').addClass('student-table-active');
              $('#myModal').modal('toggle');
              $('#myModal').modal('show');
              $('#modal-title').empty();
              $('#modal-title').append('Select an option');
              var html='<div class="text-center">';
                html+='<button type="button" style="font-size:large;" onclick="view_donations(\'0\',\'2\',\'1\')" class="btn btn-default">View Donations</button>';
                html+='<button type="button" style="font-size:large;" onclick="view_students(\'0\',\'2\',\'1\')" class="btn btn-default">View Students</button>';
              html+='</div>';
              $('#modal-body').empty();
              $('#modal-body').append(html);
              //$('#myModal').modal('hide');
          }
        });
        if((from-offset)<=0){
          from=0;
        }else{
          from = from-offset;
        }
        $('#prev-btn').attr('onclick','');
        $('#next-btn').attr('onclick','');
        $('#prev-btn').attr('onclick','get_student('+from+','+(from+offset)+')');
        $('#next-btn').attr('onclick','get_student('+to+','+(to+offset)+')');
      }
    },
    error:(e,s)=>{
      console.log(s);
    }
  });
}

function generate_modal(id){
  $('#row-id').empty();
  $('#row-id').append(id);
}

function reset_table(){
  var id = $('#row-id').text();
  $('#modal-title').empty();
  $('#back-btn').attr('style','display:none;');
  $('#modal-title').append('Select an option');
  var html='<div class="text-center">';
    html+='<button type="button" style="font-size:large;" onclick="view_donations_student(0,2,'+id+')" class="btn btn-default">View Donations</button>';
    html+='<button type="button" style="font-size:large;" onclick="view_sponsors(0,2,'+id+')" class="btn btn-default">View Sponsors</button>';
  html+='</div>';
  $('#modal-body').empty();
  $('#modal-body').append(html);
}

function download_sponsor(e,sid){
  e.preventDefault();
  sid = $('#row-id').text();
  $.ajax({
    type:'GET',
    url:'administrator/student/sponsor/'+sid+'/csv',
    success:(data)=>{
      if(data.success){
        window.location.href='/media/'+data.path;
      }else{
        alert('error downloading file');
      }
    },
    error:(e,s)=>{
      console.log(s);
      alert('error downloading file');
    }
  });
}

function view_sponsors(from,to,student_id){
  student_id = $('#row-id').text();
  from = parseInt(from);
  to = parseInt(to);
  $('#modal-title').empty();
  $('#modal-title').append('Sponsors');
  $('#back-btn').attr('style','display:block');
  $('#modal-body').empty();
  var html = '<div class="panel-body-content" style="padding-bottom:7%">';
    html+='<button type="button" onclick="download_sponsor(event,'+student_id+')" class="btn btn-info pull-right col-md-2" style="font-size:large;">Download CSV</button>';
  html+='</div>';
  html+='<table class="table" style="font-family:sans-serif" id="admin-student-sponsor-table">';
    html+='<thead>';
      html+='<tr style="background-color:#41ea41;color:white;" id="student-sponsor-table-columns">';
        html+='<th scope="col"> First Name </th>';
        html+='<th scope="col"> Last Name </th>';
        html+='<th scope="col"> Email </th>';
      html+='</tr>';
    html+='</thead>';
    html+='<tbody id="student-sponsor-table-rows">';
    html+='</tbody>';
  html+='</table>';
  $('#modal-body').append(html);
  $.ajax({
    type:'GET',
    url:'administrator/student/sponsor/'+student_id+'/'+from+'/'+to,
    success:(data)=>{
      if(data.success){
        var sponsors = JSON.parse(data.sponsors);
        const offset = 2;
        if((from-offset)<=0){
          from = 0;
        }else{
          from = from-offset;
        }
        sponsors.forEach((obj)=>{
          html = '<tr>';
            html+='<td>'+obj.sponsor__f_name+'</td>';
            html+='<td>'+obj.sponsor__l_name+'</td>';
            html+='<td>'+obj.sponsor__email+'</td>';
          html+='</tr>';
          $('#student-sponsor-table-rows').append(html);
        });
        html='<button type="button" class="btn btn-info-btn btn-lg" onclick="view_sponsors('+from+','+(from+offset)+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" class="btn btn-info-btn btn-lg pull-right" onclick="view_sponsors('+to+','+(to+offset)+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }else{
        if(data.hasOwnProperty('Error')){
          alert(data.Error);
        }else if(data.hasOwnProperty('error')){
          alert(data.error);
        }
        const offset = 2;
        html='<button type="button" class="btn btn-info-btn btn-lg" onclick="view_sponsors('+(from-(offset*2))+','+(from-offset)+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" class="btn btn-info-btn btn-lg pull-right" onclick="view_sponsors('+from+','+to+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }
    },
    error:(e,s)=>{
      console.log(s);
    }
  });
}

function download_donation_student(e,sid){
  sid = $('#row-id').text();
  e.preventDefault();
  $.ajax({
    type:'GET',
    url:'administrator/student/donations/'+sid+'/csv',
    success:(data)=>{
      if(data.success){
        window.location.href='/media/'+data.path;
      }else{
        alert('error downloading file');
      }
    },
    error:(e,s)=>{
      console.log(s);
      alert('error downloading file');
    }
  });
}

function view_donations_student(from,to,student_id){
  from = parseInt(from);
  to = parseInt(to);
  student_id = $('#row-id').text();
  $('#modal-title').empty();
  $('#modal-title').append('Sponsors');
  $('#back-btn').attr('style','display:block');
  $('#modal-body').empty();
  var html = '<div class="panel-body-content" style="padding-bottom:7%">';
    html+='<button type="button" onclick="download_donation_student(event,'+student_id+')" class="btn btn-info pull-right col-md-2" style="font-size:large;">Download CSV</button>';
  html+='</div>';
  html+='<table class="table" style="font-family:sans-serif" id="admin-student-donation-table">';
    html+='<thead>';
      html+='<tr style="background-color:#41ea41;color:white;" id="student-donation-table-columns">';
        html+='<th scope="col"> Date </th>';
        html+='<th scope="col"> Amount </th>';
        html+='<th scope="col"> First Name </th>';
        html+='<th scope="col"> Last Name </th>';
        html+='<th scope="col"> Email </th>';
        html+='<th scope="col"> Completed </th>';
      html+='</tr>';
    html+='</thead>';
    html+='<tbody id="student-donation-table-rows">';
    html+='</tbody>';
  html+='</table>';
  $('#modal-body').append(html);
  $.ajax({
    type:'GET',
    url:'administrator/student/donations/'+student_id+'/'+from+'/'+to,
    success:(data)=>{
      if(data.success){
        var donations = JSON.parse(data.donation);
        const offset = 2;
        if((from-offset)<=0){
          from = 0;
        }else{
          from = from-offset;
        }
        donations.forEach((obj)=>{
          html = '<tr>';
            html+='<td>'+obj.date+'</td>';
            html+='<td>'+obj.amount+'</td>';
            html+='<td>'+obj.sponsor__f_name+'</td>';
            html+='<td>'+obj.sponsor__l_name+'</td>';
            html+='<td>'+obj.sponsor__email+'</td>';
            html+='<td>'+obj.completed+'</td>';
          html+='</tr>';
          $('#student-donation-table-rows').append(html);
        });
        html='<button type="button" class="btn btn-info-btn btn-lg" onclick="view_donations_student('+from+','+(from+offset)+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" class="btn btn-info-btn btn-lg pull-right" onclick="view_donations_student('+to+','+(to+offset)+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }else{
        if(data.hasOwnProperty('Error')){
          alert(data.Error);
        }else if(data.hasOwnProperty('error')){
          alert(data.error);
        }
        const offset = 2;
        html='<button type="button" class="btn btn-info-btn btn-lg" onclick="view_donations_student('+(from-(offset*2))+','+(from-offset)+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" class="btn btn-info-btn btn-lg pull-right" onclick="view_donations_student('+from+','+to+','+student_id+')">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }
    },
    error:(e,s)=>{
      console.log(s);
    }
  });
}
