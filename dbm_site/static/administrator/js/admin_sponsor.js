$(document).ready(()=>{
  $.ajax({
    type: "GET",
    url:"administrator/sponsor/0/2",
    data:{},
    success: function(data){
        if(data.success){
        var d = JSON.parse(data.sponsor);
        $('#sponsor-table-rows').empty();
        d.forEach((obj)=>{
          var html = '';
          html= '<tr onclick="generate_modal(\''+obj.id+'\',\''+(obj.thanked?1:0)+'\',\''+(obj.confirm?1:0)+'\')">';
            html+='<td>'+obj.f_name+'</td>';
            html+='<td>'+obj.l_name+'</td>';
            html+='<td>'+obj.email+'</td>';
            html+='<td>'+(obj.confirm?'True':'False')+'</td>';
            html+='<td>'+(obj.thanked?'True':'False')+'</td>';
          html+='</tr>';
          $('#sponsor-table-rows').append(html);
        });
        $('#sponsor-table-rows').find('tr').click((e)=>{
          var selected = $(e.target).closest('tr').hasClass('sponsor-table-active');
          $('#sponsor-table-rows tr').removeClass('sponsor-table-active');
          if(!selected){
              $(e.target).closest('tr').addClass('sponsor-table-active');
              $('#back-btn').attr('style','display:none;');
              $('#myModal').modal('toggle');
              $('#myModal').modal('show');
              $('#modal-title').empty();
              $('#modal-title').append('Select an option');
              var row_id = $('#row-id').text();
              var s = row_id.split(',');
              var id = s[0];
              var thanked = s[1];
              thanked = parseInt(thanked);
              var confirmed = s[2];
              confirmed = parseInt(confirmed);
              var html='<div class="text-center">';
                html+='<button type="button" style="font-size:large;" onclick="view_donations(\'0\',\'2\',\'1\')" class="btn btn-default">View Donations</button>';
                html+='<button type="button" style="font-size:large;" onclick="view_students(\'0\',\'2\',\'1\')" class="btn btn-default">View Students</button>';
                html+='<button type="button" style="font-size:large;" onclick="update_information(\'1\')" class="btn btn-default">Update Information</button>';
                if(confirmed==0){
                  html+='<button type="button" style="font-size:large;" onclick="send_email(\'1\')" class="btn btn-default">Send Reminder</button>';
                }else if(thanked==0){
                  html+='<button type="button" style="font-size:large;" onclick="send_email(\'2\')" class="btn btn-default">Send Thank You</button>';
                }
              html+='</div>';
              $('#modal-body').empty();
              $('#modal-body').append(html);
              //$('#myModal').modal('hide');
          }
        });
        }

    },
    error: function(e,s){
      console.log(e);
    }
  });

  $('#search-btn').click(function(){
    var text = $('#search-text').val();
    var ed_reg = new RegExp('^[a-zA-Z]$');
    $('#search-text').val("");
    if(ed_reg.test(text) == false){
      alert('Search can only contain characters a-z or A-Z');
    }else{
          $.ajax({
            type: "GET",
            url:"administrator/sponsor/find/"+text,
            data:{},
            success: function(data){
                if(data.success){
                  var d = JSON.parse(data.sponsors);
                  console.log(d);
                $('#sponsor-table-rows').empty();
                d.forEach((obj)=>{
                  var html = '';
                  html= '<tr onclick="generate_modal(\''+obj.id+'\',\''+(obj.thanked?1:0)+'\',\''+(obj.confirm?1:0)+'\')">';
                  html+='<td>'+obj.f_name+'</td>';
                  html+='<td>'+obj.l_name+'</td>';
                  html+='<td>'+obj.email+'</td>';
                  html+='<td>'+obj.confirm+'</td>';
                  html+='<td>'+obj.thanked+'</td>';
                  html+='</tr>';
                  $('#sponsor-table-rows').append(html);
                });
                $('#sponsor-table-rows').find('tr').click((e)=>{
                  var selected = $(e.target).closest('tr').hasClass('sponsor-table-active');
                  $('#sponsor-table-rows tr').removeClass('sponsor-table-active');
                  if(!selected){
                      $(e.target).closest('tr').addClass('sponsor-table-active');
                      $('#back-btn').attr('style','display:none;');
                      $('#myModal').modal('toggle');
                      $('#myModal').modal('show');
                      $('#modal-title').empty();
                      $('#modal-title').append('Select an option');
                      var row_id = $('#row-id').text();
                      var s = row_id.split(',');
                      var id = s[0];
                      var thanked = s[1];
                      thanked = parseInt(thanked);
                      var confirmed = s[2];
                      confirmed = parseInt(confirmed);
                      var html='<div class="text-center">';
                        html+='<button type="button" style="font-size:large;" onclick="view_donations(\'0\',\'2\',\'1\')" class="btn btn-default">View Donations</button>';
                        html+='<button type="button" style="font-size:large;" onclick="view_students(\'0\',\'2\',\'1\')" class="btn btn-default">View Students</button>';
                        html+='<button type="button" style="font-size:large;" onclick="update_information(\'1\')" class="btn btn-default">Update Information</button>';
                        if(confirmed==0){
                          html+='<button type="button" style="font-size:large;" onclick="send_email(\'1\')" class="btn btn-default">Send Reminder</button>';
                        }else if(thanked==0){
                          html+='<button type="button" style="font-size:large;" onclick="send_email(\'2\')" class="btn btn-default">Send Thank You</button>';
                        }
                      html+='</div>';
                      $('#modal-body').empty();
                      $('#modal-body').append(html);
                      //$('#myModal').modal('hide');
                  }
                });
                }else{
                  alert('not found');
                  $('#sponsor-table-rows').empty();
                }

            },
            error: function(e,s){
              console.log(e);
            }
          });

      //get rows from backend
    }
  });

  $('#download-btn').click(function(e){
    e.preventDefault();
    $.ajax({
      type:'GET',
      url:'administrator/sponsor/csv',
      success:(data)=>{
        if(data.success){
          window.location.href='/media/'+data.path;
        }else{
          alert('error downloading file');
        }
      },
      error:(e,s)=>{
        console.log(s);
        alert('error downloading file');
      }
    });
  });

  $('#sponsor-table-columns th').each(function(col){
    $(this).hover(function(){$(this).addClass('focus');},function(){$(this).removeClass('focus')});
    $(this).click(function(){
      if($(this).is('.asc')){
        $(this).removeClass('asc');
        $(this).addClass('desc selected');
        sortOrder=-1;
      }else{
        $(this).addClass('asc selected');
        $(this).removeClass('desc');
        sortOrder=1;
      }
      $(this).siblings().removeClass('asc selected');
      $(this).siblings().removeClass('desc selected');
      var arrData = $('#admin-sponsor-table').find('tbody >tr:has(td)').get();
      arrData.sort(function(a,b){
        var val1 = $(a).children('td').eq(col).text().toUpperCase();
        var val2 = $(b).children('td').eq(col).text().toUpperCase();
        if($.isNumeric(val1) && $.isNumeric(val2)){
          return sortOrder == 1 ? val1-val2:val2-val1;
        }else{
          return (val1 < val2) ? -sortOrder : (val1>val2)? sortOrder:0;
        }
      });
      $.each(arrData, function(index,row){
        $('tbody').append(row);
      });
    });
  });

  $('#send-unconfirmed-email').click(function(){
    $.ajax({
      type:'GET',
      url:'administrator/sponsor/unconfirmed',
      success:(data)=>{
        if(data.success){
          alert('message sent');
        }
      },
      error:(e,s)=>{
        console.log(s);
      }
    });
  });

  $('#send-thanks-email').click(function(){
    $.ajax({
      type:'GET',
      url:'administrator/sponsor/confirmed',
      success:(data)=>{
        if(data.success){
          alert('message sent');
        }else{
          alert('error sending message');
        }
      },
      error:(e,s)=>{
        console.log(s);
        alert('error sending message');
      }
    });
  });
});

function get_sponsor(from,to){
  $.ajax({
    type: "GET",
    url:"administrator/sponsor/"+from+"/"+to,
    data:{},
    success: function(data){
      if(data.success){
        var d = JSON.parse(data.sponsor);
        //$('sponsor-table-columns').empty();
        const offset = 2;
        $('#sponsor-table-rows').empty();
        d.forEach((obj)=>{
          var html = '';
          html= '<tr onclick="generate_modal(\''+obj.id+'\',\''+(obj.thanked?1:0)+'\',\''+(obj.confirm?1:0)+'\')">';
            html+='<td>'+obj.f_name+'</td>';
            html+='<td>'+obj.l_name+'</td>';
            html+='<td>'+obj.email+'</td>';
            html+='<td>'+obj.confirm+'</td>';
            html+='<td>'+obj.thanked+'</td>';
          html+='</tr>';
          $('#sponsor-table-rows').append(html);
        });
        if((from-offset)<=0){
          from = 0;
        }else{
          from = from-offset;
        }
        $('#prev-btn').attr('onclick','');
        $('#next-btn').attr('onclick','');
        $('#next-btn').attr('onclick','get_sponsor('+to+','+(to+offset)+');');
        $('#prev-btn').attr('onclick','get_sponsor('+from+','+(from+offset)+');');
        $('#sponsor-table-rows').find('tr').click((e)=>{
          var selected = $(e.target).closest('tr').hasClass('sponsor-table-active');
          $('#sponsor-table-rows tr').removeClass('sponsor-table-active');
          if(!selected){
              $(e.target).closest('tr').addClass('sponsor-table-active');
              $('#back-btn').attr('style','display:none;');
              $('#myModal').modal('toggle');
              $('#myModal').modal('show');
              $('#modal-title').empty();
              $('#modal-title').append('Select an option');
              var row_id = $('#row-id').text();
              var s = row_id.split(',');
              var id = s[0];
              var thanked = s[1];
              thanked = parseInt(thanked);
              var confirmed = s[2];
              confirmed = parseInt(confirmed);
              var html='<div class="text-center">';
                html+='<button type="button" style="font-size:large;" onclick="view_donations(\'0\',\'2\',\'1\')" class="btn btn-default">View Donations</button>';
                html+='<button type="button" style="font-size:large;" onclick="view_students(\'0\',\'2\',\'1\')" class="btn btn-default">View Students</button>';
                html+='<button type="button" style="font-size:large;" onclick="update_information(\'1\')" class="btn btn-default">Update Information</button>';
                if(confirmed==0){
                  html+='<button type="button" style="font-size:large;" onclick="send_email(\'1\')" class="btn btn-default">Send Reminder</button>';
                }else if(thanked==0){
                  html+='<button type="button" style="font-size:large;" onclick="send_email(\'2\')" class="btn btn-default">Send Thank You</button>';
                }
              html+='</div>';
              $('#modal-body').empty();
              $('#modal-body').append(html);
              //$('#myModal').modal('hide');
          }
        });
        }

    },
    error: function(e,s){
      console.log(e);
    }
  });
}

function generate_modal(id,thanked,confirmed){
  $('#row-id').empty();
  $('#row-id').append(''+id+','+thanked+','+confirmed);
}

function send_email(id){
  id = parseInt(id);
  var row_id = $('#row-id').text();
  var d = row_id.split(',');
  var sid = d[0];
  var thanked = d[1];
  var confirmed = d[2];
  // send reminder
  if(id==1){
    $.ajax({
      type:'GET',
      url:'administrator/sponsor/unconfirmed/'+sid,
      success:(data)=>{
        if(data.success){
          alert('message sent');
          $('#row-id').empty();
          $('#row-id').append(sid,1,thanked);
        }
      },
      error:(e,s)=>{
        alert('error sending message');
        console.log(s);
      }
    });
    //send thank you
  }else if(id==2){
    $.ajax({
      type:'GET',
      url:'administrator/sponsor/confirmed/'+sid,
      success:(data)=>{
        if(data.success){
          alert('message sent');
          $('#row-id').empty();
          $('#row-id').append(sid,confirmed,1);
        }
      },
      error:(e,s)=>{
        alert('error sending message');
        console.log(s);
      }
    });
  }
  reset_table();
  $('#myModal').modal('hide');
}

function reset_table(){
  var row_id = $('#row-id').text();
  var d = row_id.split(',');
  var id = d[0];
  var thanked = d[1];
  thanked = parseInt(thanked);
  var confirmed = d[2];
  confirmed = parseInt(confirmed);
  $('#modal-title').empty();
  $('#back-btn').attr('style','display:none;');
  $('#modal-title').append('Select an option');
  var html='<div class="text-center">';
    html+='<button type="button" style="font-size:large;" onclick="view_donations(0,2,'+id+')" class="btn btn-default">View Donations</button>';
    html+='<button type="button" style="font-size:large;" onclick="view_students(0,2,'+id+')" class="btn btn-default">View Students</button>';
    html+='<button type="button" style="font-size:large;" onclick="update_information('+id+')" class="btn btn-default">Update Information</button>';
    if(confirmed==0){
      html+='<button type="button" style="font-size:large;" onclick="send_email(\'1\')" class="btn btn-default">Send Reminder</button>';
    }else if(thanked==0){
      html+='<button type="button" style="font-size:large;" onclick="send_email(\'2\')" class="btn btn-default">Send Thank You</button>';
    }
  html+='</div>';
  $('#modal-body').empty();
  $('#modal-body').append(html);
}

function update_information(sponsor_id){
  $('#modal-title').empty();
  $('#modal-title').append('Update Information');
  $('#back-btn').attr('style','display:block');
  var html='<div class="form-group">';
    html+='<label class="col-sm-2 control-label" style="font-size:large;">'+'Email'+'</label>';
    html+='<div class="col-sm-10">';
      html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor-email" placeholder="email"/>';
    html+='</div>';
  html+='</div>';
  html+='<div class="form-group">';
    html+='<label class="col-sm-2" style="font-size:large;" control-label">'+'First'+'</label>';
    html+='<div class="col-sm-10">';
      html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor-first" placeholder="First"/>';
    html+='</div>';
  html+='</div>';
  html+='<div class="form-group">';
    html+='<label class="col-sm-2" style="font-size:large;" control-label">'+'Last'+'</label>';
    html+='<div class="col-sm-10">';
      html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor-last" placeholder="Last"/>';
    html+='</div>';
  html+='</div>';
  $('#modal-body').empty();
  $('#modal-body').append(html);
  html='<button type="button" style="margin-top:2%;margin-left:77%;" onclick="update_sponsor('+sponsor_id+')" class="btn btn-success btn-lg">';
    html+='Update Information';
  html+='</button>';
  $('#modal-body').append(html);
  $.ajax({
    type:'GET',
    url:'administrator/sponsor/update/'+sponsor_id,
    success:(data)=>{
      console.log(data);
      if(data.success){
        $('#sponsor-email').val(''+data.email);
        $('#sponsor-first').val(''+data.f_name);
        $('#sponsor-last').val(''+data.l_name);
      }
    },
    error:(e,s)=>{
      console.log(s);
    }
  });

}

function update_sponsor(sponsor_id){
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie !== '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) === (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');
  function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
  });
  var f_name = $('#sponsor-first').val();
  var last_name = $('#sponsor-last').val();
  var email = $('#sponsor-email').val();
  $.ajax({
    type:'POST',
    url:'administrator/sponsor/update/'+sponsor_id,
    data:{
      'f_name':f_name,
      'l_name':last_name,
      'email':email
    },
    success:(data)=>{
      console.log(data);
      if(data.success){
        reset_table();
      }
    },
    error:(e,s)=>{
      console.log(s);
    }
  });
}

function download_student(e,sponsor_id){
  e.preventDefault();
  var row_id=$('#row-id').text();
  var s = row_id.split(',');
  sponsor_id = s[0];
  $.ajax({
    type:'GET',
    url:'administrator/sponsor/student/'+sponsor_id+'/csv',
    success:(data)=>{
      if(data.success){
        window.location.href='/media/'+data.path;
      }else{
        alert('error downloading file');
      }
    },
    error:(e,s)=>{
      console.log(s);
      alert('error donwloading csv');
    }
  });
}

function view_students(from,to,sponsor_id){
  var row_id = $('#row-id').text();
  var s = row_id.split(',');
  sponsor_id = s[0];
  $('#modal-title').empty();
  $('#modal-title').append('Students');
  $('#back-btn').attr('style','display:block;');
  $('#modal-body').empty();
  from = parseInt(from);
  to = parseInt(to);
  var html='<div class="panel-body-content" style="padding-bottom:7%;">';
    html+='<button type="button" onclick="download_student(event,'+sponsor_id+')" class="btn btn-info pull-right col-md-2" style="font-size:large;">Download CSV</button>';
  html+='</div>';
  html+= '<table class="table" style="font-family:sans-serif" id="admin-sponsor-donation-table">';
    html+='<thead>';
      html+='<tr style="background-color:#41ea41;color:white;" id="sponor-donation-table-columns">';
        html+='<th scope="col">'+'Amount'+'</th>';
        html+='<th scope="col">'+'Date'+'</th>';
        html+='<th scope="col">'+'First Name'+'</th>';
        html+='<th scope="col">'+'Last Name'+'</th>';
      html+='</tr>';
    html+='</thead>';
    html+='<tbody id="sponsor-student-table-rows">';
    html+='</tbody>';
  html+'</table>';
  $('#modal-body').append(html);
  $.ajax({
    type:'GET',
    url:'administrator/sponsor/student/'+sponsor_id+'/'+from+'/'+to,
    success:(data)=>{
      console.log(data);
      if(data.success){
        var students = JSON.parse(data.student);
        const offset=2;
        if((from-offset)<=0){
          from = 0;
        }else{
          from = from-offset;
        }
        students.forEach((obj)=>{
          html = '<tr>';
            html+='<td>'+obj.amount+'</td>';
            html+='<td>'+obj.date+'</td>';
            html+='<td>'+obj.student__user__first_name+'</td>';
            html+='<td>'+obj.student__user__last_name+'</td>';
          html+='</tr>';
          $('#sponsor-student-table-rows').append(html);
        });
        html='<button type="button" class="btn btn-info-btn btn-lg" onclick="view_students('+from+','+(from+offset)+','+sponsor_id+')">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" class="btn btn-info-btn btn-lg pull-right" onclick="view_students('+to+','+(to+offset)+','+sponsor_id+')">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }else{
        if(data.hasOwnProperty("Error")){
          alert(data.Error);
        }else if(data.hasOwnProperty("error")){
          alert(data.error);
        }
        const offset = 2;
        html='<button type="button" class="btn btn-info-btn btn-lg" onclick="view_students('+(from-(offset*2))+','+(from-offset)+','+sponsor_id+')">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" class="btn btn-info-btn btn-lg pull-right" onclick="view_students('+from+','+to+','+sponsor_id+')">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }
    },
    error:(e,s)=>{
      alert('this person has no students');
      console.log(e);
    }
  });

}

function download_donation(e,sponsor_id){
  e.preventDefault();
  var row_id=$('#row-id').text();
  var s = row_id.split(',');
  sponsor_id = s[0];
  $.ajax({
    type:'GET',
    url:'administrator/sponsor/donation/'+sponsor_id+'/csv',
    success:(data)=>{
      if(data.success){
        window.location.href='/media/'+data.path;
      }else{
        alert('error downloading file');
      }
    },
    error:(e,s)=>{
      console.log(s);
      alert('error donwloading csv');
    }
  });
}

function view_donations(from,to,sponsor_id){
  var row_id=$('#row-id').text();
  var s = row_id.split(',');
  sponsor_id = s[0];
  $('#modal-title').empty();
  $('#modal-title').append('Donations');
  $('#back-btn').attr('style','display:block;');
  $('#modal-body').empty();
  from = parseInt(from);
  to = parseInt(to)
  var html='<div class="panel-body-content" style="padding-bottom:7%;">';
    html+='<button type="button" id="download-donation-btn" onclick="download_donation(event,\''+sponsor_id+'\')" class="btn btn-info pull-right col-md-2" style="font-size:large;">Download CSV</button>';
  html+='</div>';
  $('#modal-body').append(html);
  html= '<table class="table" style="font-family:sans-serif" id="admin-sponsor-donation-table">';
    html+='<thead>';
      html+='<tr style="background-color:#41ea41;color:white;" id="sponor-donation-table-columns">';
        html+='<th scope="col">'+'date'+'</th>';
        html+='<th scope="col">'+'amount'+'</th>';
        html+='<th scope="col">'+'first name'+'</th>';
        html+='<th scope="col">'+'last name'+'</th>';
        html+='<th scope="col">'+'completed'+'</th>';
      html+='</tr>';
    html+='</thead>';
    html+='<tbody id="sponsor-donation-table-rows">';
    html+='</tbody>';
  html+='</table>';
  $('#modal-body').append(html);
  $.ajax({
    type:'GET',
    url:'administrator/sponsor/donation/'+sponsor_id+'/'+from+'/'+to,
    success:(data)=>{
      console.log(data);
      $('#sponsor-donation-table-rows').empty();
      console.log(from);
      console.log(to);
      if(data.success){
        var donation = JSON.parse(data.donation);
        console.log(donation);
        donation.forEach((obj)=>{
          var html = '';
          html+='<tr>';
            html+='<td>'+obj.date+'</td>';
            html+='<td>'+obj.amount+'</td>';
            html+='<td>'+obj.student__user__first_name+'</td>';
            html+='<td>'+obj.student__user__last_name+'</td>';
            html+='<td>'+(obj.completed?'True':'False')+'</td>';
          html+='</tr>';
          $('#sponsor-donation-table-rows').append(html);
        });
        const offset=2;
        if((from-offset)<=0){
          from = 0;
        }else{
          from = from-offset;
        }
        html='<button type="button" onclick="view_donations('+from+','+(from+offset)+','+sponsor_id+')" class="btn btn-info-btn btn-lg" id="donation-prev-btn">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" onclick="view_donations('+to+','+(to+offset)+','+sponsor_id+')" class="btn btn-info-btn btn-lg pull-right">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }else{
        if(data.hasOwnProperty("Error")){
          alert(data.Error);
        }else if(data.hasOwnProperty("error")){
          alert(data.error);
        }
        const offset = 2;
        html='<button type="button" onclick="view_donations('+(from-(offset*2))+','+(from-offset)+','+sponsor_id+')" class="btn btn-info-btn btn-lg" id="donation-prev-btn">';
          html+='<span class="glyphicon glyphicon-step-backward"></span>';
        html+='</button>';
        html+='<button type="button" onclick="view_donations('+from+','+to+','+sponsor_id+')" class="btn btn-info-btn btn-lg pull-right">';
          html+='<span class="glyphicon glyphicon-step-forward"></span>';
        html+='</button>';
        $('#modal-body').append(html);
      }
    },
    error:(e,s)=>{
      alert('This sponsor has no donations');
      console.log(e);
    }
  });

}
