$(document).ready(()=>{
  $.ajax({
    type: "GET",
    url:"student/sponsor/0/2",
    data:{},
    success: function(data){
      if(data.success){
        var d = JSON.parse(data.sponsors);
        $('#sponsor-table-rows').empty();
        d.forEach((obj)=>{
          var html = '';
          html= '<tr>';
            html+='<td>'+obj.date+'</td>';
            html+='<td>'+obj.amount+'</td>';
            html+='<td>'+obj.sponsor__f_name+'</td>';
            html+='<td>'+obj.sponsor__l_name+'</td>';
            html+='<td>'+obj.sponsor__email+'</td>';
            html+='<td>'+obj.sponsor__phone+'</td>';
            html+='<td>'+(obj.sponsor__confirm?'True':'False')+'</td>';
          html+='</tr>';
          $('#sponsor-table-rows').append(html);
        });
        $('#sponsor-table-rows').find('tr').click((e)=>{
          var selected = $(e.target).closest('tr').hasClass('sponsor-table-active');
          $('#sponsor-table-rows tr').removeClass('sponsor-table-active');
          if(!selected){
              $(e.target).closest('tr').addClass('sponsor-table-active');
          }
        });
      }else{
        $('#sponsor-table-rows').empty();
        alert('You have no sponsors');
      }

    },
    error: function(e,s){
      console.log(e);
    }
  });

  $('#download-btn').click((e)=>{
    e.preventDefault();
    $.ajax({
      type:'GET',
      url:'student/download',
      success:(data)=>{
        if(data.success){
          window.location.href='/media/'+data.path;
        }else{
          alert('error downloading csv');
        }
      },
      error:(e,s)=>{
        console.log(s);
        alert('error donwloading csv');
      }
    });
  });

  $('#add-sponsor-btn').click(function(){
    $('#myModal').modal('toggle');
    $('#myModal').modal('show');
    $('#modal-title').empty();
    $('#modal-title').append('Add a Sponsor');
    var html = '';
    html+='<div class="form-group">';
      html+='<label class="col-sm-2 control-label" style="font-size:large;">'+'First Name'+'</label>';
      html+='<div class="col-sm-10">';
        html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor_first_name" placeholder="First Name"/>';
      html+='</div>';
    html+='</div>';
    html+='<div class="form-group">';
      html+='<label class="col-sm-2 control-label" style="font-size:large;">'+'Last Name'+'</label>';
      html+='<div class="col-sm-10">';
        html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor_last_name" placeholder="Last Name"/>';
      html+='</div>';
    html+='</div>';
    html+='<div class="form-group">';
      html+='<label class="col-sm-2 control-label" style="font-size:large;">'+'Email'+'</label>';
      html+='<div class="col-sm-10">';
        html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor_email" placeholder="Email"/>';
      html+='</div>';
    html+='</div>';
    html+='<div class="form-group">';
      html+='<label class="col-sm-2 control-label" style="font-size:large;">'+'Phone'+'</label>';
      html+='<div class="col-sm-10">';
        html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor_phone" placeholder="(XXX)xxx-xxxxx"/>';
      html+='</div>';
    html+='</div>';
    html+='<div class="form-group">';
      html+='<label class="col-sm-2 control-label" style="font-size:large;">'+'Amount'+'</label>';
      html+='<div class="col-sm-10">';
        html+='<input type="text" style="font-size:large;" class="form-control" id="sponsor_amount" placeholder="xx.xx"/>';
      html+='</div>';
    html+='</div>';
    $('#modal-body').empty();
    $('#modal-body').append(html);
    html='<button type="button" style="margin-top:2%;margin-left:77%;" onclick="add_sponsor()" class="btn btn-success btn-lg">';
      html+='Add Sponsor';
    html+='</button>';
    $('#modal-body').append(html);
  });

  $('#sponsor-table-columns th').each(function(col){
    $(this).hover(function(){$(this).addClass('focus');},function(){$(this).removeClass('focus')});
    $(this).click(function(){
      if($(this).is('.asc')){
        $(this).removeClass('asc');
        $(this).addClass('desc selected');
        sortOrder=-1;
      }else{
        $(this).addClass('asc selected');
        $(this).removeClass('desc');
        sortOrder=1;
      }
      $(this).siblings().removeClass('asc selected');
      $(this).siblings().removeClass('desc selected');
      var arrData = $('#admin-sponsor-table').find('tbody >tr:has(td)').get();
      arrData.sort(function(a,b){
        var val1 = $(a).children('td').eq(col).text().toUpperCase();
        var val2 = $(b).children('td').eq(col).text().toUpperCase();
        if($.isNumeric(val1) && $.isNumeric(val2)){
          return sortOrder == 1 ? val1-val2:val2-val1;
        }else{
          return (val1 < val2) ? -sortOrder : (val1>val2)? sortOrder:0;
        }
      });
      $.each(arrData, function(index,row){
        $('tbody').append(row);
      });
    });
  });
});

function add_sponsor(){
  var first_name = $('#sponsor_first_name').val();
  var last_name = $('#sponsor_last_name').val();
  var email = $('#sponsor_email').val();
  var phone = $('#sponsor_phone').val();
  var amount = $('#sponsor_amount').val();
  if(first_name=="" || last_name=="" || email=="" || phone=="" || amount==""){
    alert('fields cant be empty');
  }else{
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.ajax({
      type:'POST',
      url:"student/sponsor/0/0",
      data:{
        'f_name':first_name,
        'l_name':last_name,
        'email':email,
        'phone':phone,
        'amount':amount
      },
      success:(data)=>{
        if(data.success){
          $('#sponsor_first_name').val('');
          $('#sponsor_last_name').val('');
          $('#sponsor_email').val('');
          $('#sponsor_phone').val('');
          $('#sponsor_amount').val('');
          alert('sponsor added successfully');
          $('#myModal').modal('hide');
        }
      },
      error:(e,s)=>{
        console.log(s);
      }
    });
  }
}

function get_sponsor(from,to){
  $.ajax({
    type: "GET",
    url:"student/sponsor/"+from+"/"+to,
    data:{},
    success: function(data){
      if(data.success){
        var d = JSON.parse(data.sponsors);
        //$('sponsor-table-columns').empty();
        const offset = 2;
        $('#sponsor-table-rows').empty();
        d.forEach((obj)=>{
          var html = '';
          html= '<tr>';
            html+='<td>'+obj.date+'</td>';
            html+='<td>'+obj.amount+'</td>';
            html+='<td>'+obj.sponsor__f_name+'</td>';
            html+='<td>'+obj.sponsor__l_name+'</td>';
            html+='<td>'+obj.sponsor__email+'</td>';
            html+='<td>'+obj.sponsor__phone+'</td>';
            html+='<td>'+(obj.sponsor__confirm?'True':'False')+'</td>';
          html+='</tr>';
          $('#sponsor-table-rows').append(html);
        });
        if((from-offset)<=0){
          from = 0;
        }else{
          from = from-offset;
        }
        $('#prev-btn').attr('onclick','');
        $('#next-btn').attr('onclick','');
        $('#next-btn').attr('onclick','get_sponsor('+to+','+(to+offset)+');');
        $('#prev-btn').attr('onclick','get_sponsor('+from+','+(from+offset)+');');
        $('#sponsor-table-rows').find('tr').click((e)=>{
          var selected = $(e.target).closest('tr').hasClass('sponsor-table-active');
          $('#sponsor-table-rows tr').removeClass('sponsor-table-active');
          if(!selected){
              $(e.target).closest('tr').addClass('sponsor-table-active');
          }
        });
        }

    },
    error: function(e,s){
      console.log(e);
    }
  });
}
