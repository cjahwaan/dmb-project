$( document ).ready(function() {

  $.ajax({
    type: 'GET',
    url: 'student/notifications',
    success: function(data){
      console.log(data);
      if(data.success){
        var notifications = JSON.parse(data.notifications);
        $('#notif-list').empty();
        notifications.forEach(function(obj){
          var html = '';
          html+='<li class="list-group-item notif-item">'+obj.message+'</li>';
          $('#notif-list').append(html);
        });
      }else{
        alert('Notification failed to send');
      }
    },
    error:function(e,s){
      console.log(s);
      alert('Notification failed to send');
    }
  });

  $.get('/static/student/html/home.html',(r)=>{
    $('#panel-body').empty();
      $('#panel-body').append(r);
      $('#student-tab').removeClass('sidebar-item-active');
      $('#home-tab').addClass('sidebar-item-active');
      $('#sponsor-tab').removeClass('sidebar-item-active');
      $('#home-icon').attr('style','font-size:xx-large;color:white');
      $('#student-icon').attr('style','font-size:xx-large;color:#999999');
      $('#sponsor-icon').attr('style','font-size:xx-large;color:#999999');
  });

  $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("active");
  });

    $(".setsize").each(function() {
        $(this).height($(this).width());
    });

    $('#home-btn').click(()=>{
      $('#panel-title').empty();
      $('#panel-title').append('Statistics');
      $.get('/static/student/html/home.html',(r)=>{
        $('#panel-body').empty();
        $('#panel-body').append(r);
        $('#student-tab').removeClass('sidebar-item-active');
        $('#home-tab').addClass('sidebar-item-active');
        $('#sponsor-tab').removeClass('sidebar-item-active');
        $('#home-icon').attr('style','font-size:xx-large;color:white');
        $('#student-icon').attr('style','font-size:xx-large;color:#999999');
        $('#sponsor-icon').attr('style','font-size:xx-large;color:#999999');
      });
    });

    $('#sponsor-btn').click(()=>{
      $('#panel-title').empty();
      $('#panel-title').append('Sponsors');
      $.get('/static/student/html/student_sponsor.html',(r)=>{
        $('#panel-body').empty();
        $('#panel-body').append(r);
        $('#student-tab').removeClass('sidebar-item-active');
        $('#home-tab').removeClass('sidebar-item-active');
        $('#sponsor-tab').addClass('sidebar-item-active');
        $('#home-icon').attr('style','font-size:xx-large;color:#999999');
        $('#student-icon').attr('style','font-size:xx-large;color:#999999');
        $('#sponsor-icon').attr('style','font-size:xx-large;color:white');
      });
    });

    $('#send-btn').click(()=>{
      var text = $('#send-text').val();
      $('#send-text').val('');
      function getCookie(name) {
          var cookieValue = null;
          if (document.cookie && document.cookie !== '') {
              var cookies = document.cookie.split(';');
              for (var i = 0; i < cookies.length; i++) {
                  var cookie = jQuery.trim(cookies[i]);
                  // Does this cookie string begin with the name we want?
                  if (cookie.substring(0, name.length + 1) === (name + '=')) {
                      cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                      break;
                  }
              }
          }
          return cookieValue;
      }
      var csrftoken = getCookie('csrftoken');
      function csrfSafeMethod(method) {
          // these HTTP methods do not require CSRF protection
          return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
      }
      $.ajaxSetup({
          beforeSend: function(xhr, settings) {
              if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                  xhr.setRequestHeader("X-CSRFToken", csrftoken);
              }
          }
      });
      $.ajax({
        type:'POST',
        url: 'student/notifications',
        data: {
          'message': text
        },
        success: function(data){
          console.log(data);
          if(data.success){
            var updates = {};
            updates['notification']=true;
            database.ref().update(updates);
          }else{
            alert('Notification failed to send');
          }
        },
        error:function(e,s){
          console.log(s);
          alert('Notification failed to send');
        }
      });

    });

    var notifications = database.ref('notification');
    notifications.on('value',(snapshot)=>{
      if(snapshot.val()==true){
        $.ajax({
          type: 'GET',
          url: 'student/notifications',
          success: function(data){
            console.log(data);
            if(data.success){
              var notifications = JSON.parse(data.notifications);
              $('#notif-list').empty();
              notifications.forEach(function(obj){
                var html = '';
                html+='<li class="list-group-item notif-item">'+obj.message+'</li>';
                $('#notif-list').append(html);
              });
              var updates = {};
              updates['notification']=false;
              database.ref().update(updates);
            }else{
              alert('Notification failed to send');
            }
          },
          error:function(e,s){
            console.log(s);
            alert('Notification failed to send');
          }
        });

      }
    });

    $('#log-out-btn').click(()=>{
      $.ajax({
        url: '/users/logout',
        type:'GET',
        success:(data)=>{
          if(data.success){
            window.location.replace('/');
          }
        },
        error:(e)=>{
          console.log(e);
        }
      });
    });
});
