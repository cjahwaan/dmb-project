$(document).ready(()=>{
  $(".setsize").each(function() {
      $(this).height($(this).width());
  });
  $.ajax({
    type: "GET",
    url:"student/donations",
    success: function(data){
      console.log(data);
        console.log($('#percent').text());
        $('#percent').empty();
        var newPercent = (1-(data.tot_unconfirmed_sponsor / data.tot_sponsor))*100;
        $('#percent').append(newPercent.toFixed(2) + '%');
        newPercent = Math.round(newPercent/5)*5;
        $('#pie-chart').removeClass('progress-90');
        $('#pie-chart').addClass('progress-' + newPercent);
        $('#percent-given').empty();
        var newPercent2 = (1-(data.unconfirmed / data.total))*100;
        $('#percent-given').append(newPercent2.toFixed(2) + '%');
        newPercent2 = Math.round(newPercent2/5)*5;
        console.log(newPercent2);
        $('#pie-chart2').removeClass('progress-90');
        $('#pie-chart2').addClass('progress-' + newPercent2);
        console.log(data);
        $('#num-Donors').empty();
        $('#num-Donors').append((data.tot_sponsor - data.tot_unconfirmed_sponsor) + ' of ' + data.tot_sponsor +' sponsors confirmed');
        $('#num-Given').empty();
        $('#num-Given').append('$'+(data.total-data.unconfirmed) + ' collected of $' + data.total +' pledged');
        var moneyPercent = ((data.total - data.unconfirmed)/data.goal)*100;
        $('#money-progress').attr('style', 'width:' + moneyPercent + '%;font-size:xx-large;padding-top:2%; color:black;');
        $('#money-progress').empty();
        $('#money-progress').append(moneyPercent + '%');
        var currentCollected = data.total - data.unconfirmed;
        $('#collected').empty();
        $('#collected').append('$'+currentCollected);
        $('#total').empty();
        $('#total').append('$' + data.goal);

    },
    error: function(e,s){
        console.log(e);
    }

    });

});

$(window).on('resize', function(){
    $(".setsize").each(function() {
        $(this).height($(this).width());
    });
});
