$(document).ready(()=>{
  $('#login_btn').click(()=>{
    const email = $('#login_email').val();
    const password = $('#login_password').val();
    var emailreg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(!emailreg.test(email) || email==""){
      alert('Enter a valid email');
    }else{
      function getCookie(name) {
          var cookieValue = null;
          if (document.cookie && document.cookie !== '') {
              var cookies = document.cookie.split(';');
              for (var i = 0; i < cookies.length; i++) {
                  var cookie = jQuery.trim(cookies[i]);
                  // Does this cookie string begin with the name we want?
                  if (cookie.substring(0, name.length + 1) === (name + '=')) {
                      cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                      break;
                  }
              }
          }
          return cookieValue;
      }
      var csrftoken = getCookie('csrftoken');
      function csrfSafeMethod(method) {
          // these HTTP methods do not require CSRF protection
          return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
      }
      $.ajaxSetup({
          beforeSend: function(xhr, settings) {
              if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                  xhr.setRequestHeader("X-CSRFToken", csrftoken);
              }
          }
      });
      $.ajax({
        url: '/users/login',
        type:'POST',
        data:{
          email: email,
          password: password
        },
        success: (data)=>{
          console.log(data);
          if(data.success){
            window.location.replace('/');
          }else{
            alert('Wrong credentials');
          }
        },
        error:(rs,e)=>{
          console.log(rs)
          console.log(e)
        }
      });
    }
  });
});
