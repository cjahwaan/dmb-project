$(document).ready(()=>{
  var path = window.location.pathname;
  var s = path.split('/');
  //pathname=/sponsor/id/token
  var id = s[2];
  var token = s[3];
  var donation_id = s[4];
  console.log(s);
  $.ajax({
    type:'GET',
    url:'/sponsor/'+id+'/'+token+'/'+donation_id+'/confirm',
    success:(data)=>{
      if(!data.success){
        alert('error confirming token');
      }else{
        $.ajax({
          type:'GET',
          url:'/sponsor/'+id+'/'+token+'/'+donation_id+'/donation',
          success:(data)=>{
            if(data.success){
              var student = ''+data.student_first_name+' '+data.student_last_name;
              $('#student').val(student);
              $('#amount').val(data.amount);
            }else{
              alert('error retrieving student information');
            }
          },
          error:(e,s)=>{
            console.log(s);
            alert('error retrieving student information');
          }
        });
      }
    },
    error:(c,s)=>{
      console.log(s);
      alert('error confirming your token');
    }
  });

  $('#confirm-btn').click(()=>{
    var mayContinue = true;
    var amount = $('#amount').val();
    if($.isNumeric(amount)==false){
		    mayContinue = false;
  	}else{
  	   amount = parseFloat(amount);
       amount.toFixed(2);
  	}

    var ccn = $('#ccn').val();
    if (ccn.length > 3 || ccn.length < 3){
			mayContinue = false;
			alert('invalid ccn number');
		}else{
			if($.isNumeric(ccn)==false){
				mayContinue = false;
				alert('ccn must be number');
			}
		}
    var card_number = $('#card_number').val();
    if(card_number.length > 16 || card_number.length < 16){
		    mayContinue = false;
        alert('invalid credit card number');
	  }else{
		    if($.isNumeric(card_number)==false){
			       mayContinue = false;
	           alert('card number must be a number');
        }
        var expiration_date = $('#expiration_date').val();
        $('#amount').val('');
        $('#ccn').val('');
        $('#card_number').val('');
        $('#expiration_date').val('');

        var ed_reg = new RegExp('^[0-9][0-9]\/[0-9][0-9]$');
        if(ed_reg.test(expiration_date) == false)
        {
    		    mayContinue = false;
    		    alert('invalid expiration date');
    	  }
        if(ccn=="" || card_number=="" || expiration_date==""){
          alert('fields cant be empty');
        }else{
		        if(mayContinue == false){
			           alert('invalid credit card information');
            }else{
              function getCookie(name) {
                  var cookieValue = null;
                  if (document.cookie && document.cookie !== '') {
                      var cookies = document.cookie.split(';');
                      for (var i = 0; i < cookies.length; i++) {
                          var cookie = jQuery.trim(cookies[i]);
                          // Does this cookie string begin with the name we want?
                          if (cookie.substring(0, name.length + 1) === (name + '=')) {
                              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                              break;
                          }
                      }
                  }
                  return cookieValue;
              }
              var csrftoken = getCookie('csrftoken');
              function csrfSafeMethod(method) {
                  // these HTTP methods do not require CSRF protection
                  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
              }
              $.ajaxSetup({
                  beforeSend: function(xhr, settings) {
                      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                          xhr.setRequestHeader("X-CSRFToken", csrftoken);
                      }
                  }
              });
              $.ajax({
                type:'POST',
                url:'/sponsor/'+id+'/'+token+'/'+donation_id+'/donation',
                data:{
                  amount:amount
                },
                success:(data)=>{
                  if(data.success){
                    alert('donation completed');
                    $('#back-btn').attr('style','display:none;');
                  	$('#myModal').modal('toggle');
          	        $('#myModal').modal('show');
          	        $('#modal-title').empty();
          	        $('#modal-title').append('Make Another Donation?');
          	        var html='<div class="text-center">';
          	        html+='<button type="button" style="font-size:large;margin-right:5%;padding-left:5%;padding-right:5%;" onclick="no_msg()" class="btn btn-danger btn-lg" id="confirm-btn">No</button>';
          	        html += '<button type="button" style="font-size:large;padding-left:5%;padding-right:5%;" style="font-size:large;" class="btn btn-success btn-lg" onclick="another_donation()">Yes</button>';
          	        html+='</div>';
          	        $('#modal-body').empty();
          	        $('#modal-body').append(html);
                  }else{
                    alert('error while completing donation');
                  }
                },
                error:(e,s)=>{
                  console.log(s);
                  alert('error while completing donation');
                }
              });

           }
       }
     }
  });
  $('#error-btn').click(()=>{
    //send error
    $('#sponsor-container').empty();
    var html = '<div style="font-size:x-large;font-weight:bold;text-align:center;">';
      html+='What is the error?'
    html+='</div>';
    html+='<div class="form-group">';
      html+='<textarea class="form-control" rows="5" id="error-report"></textarea>';
    html+='</div>';
    html+='<div class="text-center" style="margin-top:2%;">';
      html+='<button type="button" class="btn btn-success btn-lg" onclick="send_error()">';
        html+='Send Error Report'
      html+='</div>';
    html+='</div>';
    $('#sponsor-container').append(html);

  });
});

function send_error(){
  var path = window.location.pathname;
  var s = path.split('/');
  //pathname=/sponsor/id/token
  var id = s[2];
  var token = s[3];
  var donation_id = s[4];
  var text = $('#error-report').val();
  $('#sponsor-container').empty();
  function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie !== '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = jQuery.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) === (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');
  function csrfSafeMethod(method) {
      // these HTTP methods do not require CSRF protection
      return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
  });
  $.ajax({
    type:'POST',
    url:'/sponsor/'+id+'/'+token+'/'+donation_id+'/error',
    data:{
      'text':text
    },
    success:(data)=>{
      if(data.success){
        var html = '<div class="text-center" style="font-size:x-large;font-weight:bold;">';
        html+='Thank You For Notifying Us. We will fix this shortly';
        html+='</div>';
        $('#sponsor-container').append(html);
      }else{
        alert('error sending message');
      }
    },
    error:(e,s)=>{
      console.log(s);
      alert('error sending message');
    }
  });
}

function another_donation(){
  $('#modal-body').empty();
  $('#myModal').modal('hide');
  $('#amount').val('');
  $('#ccn').val('');
  $('#card_number').val('');
  $('#expiration_date').val('');
}

function no_msg(){
  $('#modal-body').empty();
  $('#myModal').modal('hide');
  $('#sponsor-container').empty();
  var html = '<div class="text-center" style="font-size:x-large;font-weight:bold;">';
  html+='Thank You For Your Donation';
  html+='</div>';
  $('#sponsor-container').append(html);
}
