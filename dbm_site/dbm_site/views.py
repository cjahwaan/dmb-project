from django.shortcuts import render
from django.http import JsonResponse
import json
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin as LR
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.utils.crypto import get_random_string
from datetime import datetime, timedelta
from django.core.mail import send_mail
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect
from django.conf import settings

@login_required(login_url='/auth')
def index(request):
    if request.user.groups.filter(name='Administrator').exists() is True:
        return render(request,'administrator/html/index.html')
    elif request.user.groups.filter(name='Student').exists() is True:
        return render(request,'student/html/index.html')
    else:
        return JsonResponse({'Error':'Unauthorized access'})

def auth(request):
    if request.user.is_authenticated:
        return redirect('/')
    return render(request,'home/html/auth.html')

class Login(View):
    # POST = logs user in
    # takes params email, password
    # returns True if success
    def post(self,request):
        email = request.POST['email']
        password = request.POST['password']
        # check if user doesn't exists
        if User.objects.filter(username=email).exists() is False:
            # if user doesn't exist return error message and success false
            return JsonResponse({'success':False,'Error':'Email doesnt exist'})
        else:
            #if user exists then authenticate the user
            user = authenticate(username=email,password=password)
            # if user credentials are valid
            if user is not None:
                #login the user
                login(request,user)
                #return true
                return JsonResponse({'success':True})
            else:
                #if credentials are invalid then return false and error
                return JsonResponse({'success':False,'Error':'Invalid Credentials'})

class Logout(LR,View):
    login_url='/auth'
    # GET = logouts user
    # takes no params
    # returns True if success
    def get(self,request):
        logout(request)
        return JsonResponse({'success':True})
